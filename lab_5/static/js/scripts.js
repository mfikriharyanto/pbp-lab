$(function() {
    $(document).on("click", ".view-note", function() {
        var selector = $(this).data('selector');
        var title = "<h3><b>" + $("#"+selector+" .title").text() +"</b></h3>";
        $("#modal-note .modal-header").html(title);

        var body = "<p>From: " + $("#"+selector+" .from").text();
        body += "<br>To: " + $("#"+selector+" .to").text() + "</p>";
        body += $("#"+selector+" .message").html();
        $("#modal-note .modal-body").html(body);

        $("#button2").remove();
    });

    $(document).on("click", ".edit-note", function() {
        var id = $(this).data('selector');
        title = $("#" + id).find(".title").text();
        from = $("#" + id).find(".from").text();
        to = $("#" + id).find(".to").text();
        message = $("#" + id).find(".message").text();
        $('#id_title').val(title);
        $('#id_note_from').val(from);
        $('#id_note_to').val(to);
        $('#id_message').val(message);
    });

    $(document).on("click", ".del-next", function() {
        var id = $(this).data('selector');
        var action = confirm("Are you sure you want to delete this Note?");
        if (action != false) {
          $.ajax({
              url: 'delete-note',
              data: {
                  'id': id,
              },
              dataType: 'json',
              success: function (data) {
                var action = confirm("Are you sure?");
                  // if (data.deleted) {
                  //   $("#table-note #" + id).remove();
                  // }
              }
          });
        }
    });

    $(document).on("click", ".del-note", function() {
        var id = $(this).data('selector');

        var header = "<h3><b>Delete Note</b></h3>";
        $("#modal-note .modal-header").html(header);

        var selector = $(this).data('selector');
        var body = "<p> Apakah kamu yakin untuk menghapus note ini? </p>";
        body += "<p><b>" + $("#"+selector+" .title").text() +"</b></p>";
        body += "<p>From: " + $("#"+selector+" .from").text();
        body += "<br>To: " + $("#"+selector+" .to").text() + "</p>";
        body += $("#"+selector+" .message").html();
        $("#modal-note .modal-body").html(body);

        var button = "<button id='button2' class='btn btn-danger del-next' data-selector='{{ id }}'>Delete</button>";
        $("#button2").remove();
        $("#button-close").before(button);
    });

    $(document).on("click", ".test", function() {
        var body = "<p><b> Note berhasil dihapus </b></p>";
        $("#modal-note .modal-body").html(body);
        // var titleInput = $('input[name="title"]').val().trim();
        // var fromInput = $('input[name="note_from"]').val().trim();
        // var toInput = $('input[name="note_to"]').val().trim();
        // var messageInput = $('input[name="message"]').val().trim();
        // if (titleInput && fromInput && toInput && messageInput) {
        //     // Create Ajax Call
        //     $.ajax({
        //         url: '{% url "edit_note" %}',
        //         data: {
        //             'id_title': titleInput,
        //             'id_note_from': fromInput,
        //             'id_note_to': toInput,
        //             'id_message': messageInput
        //         },
        //         dataType: 'json',
        //         success: function (data) {
        //             // if (data.user) {
        //             //   updateToUserTabel(data.user);
        //             // }
        //             alert("success");
        //         }
        //     });
        // }
        // $('form#updateUser').trigger("reset");
        // $('#myModal').modal('hide');
        // return false;
    });
});

function updateToUserTabel(user){
    $("#userTable #user-" + user.id).children(".userData").each(function() {
        var attr = $(this).attr("name");
        if (attr == "name") {
          $(this).text(user.name);
        } else if (attr == "address") {
          $(this).text(user.address);
        } else {
          $(this).text(user.age);
        }
      });
}
function appendToUsrTable(user) {
  $("#userTable > tbody:last-child").append(`
        <tr id="user-${user.id}">
            <td class="userName" name="name">${user.name}</td>
            '<td class="userAddress" name="address">${user.address}</td>
            '<td class="userAge" name="age">${user.age}</td>
            '<td align="center">
                <button class="btn btn-success form-control" onClick="editUser(${user.id})" data-toggle="modal" data-target="#myModal")">EDIT</button>
            </td>
            <td align="center">
                <button class="btn btn-danger form-control" onClick="deleteUser(${user.id})">DELETE</button>
            </td>
        </tr>
    `);
}