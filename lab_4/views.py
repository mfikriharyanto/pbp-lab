from django.shortcuts import render, redirect
from .forms import NoteForm
from lab_2.models import Note

def index(request):
    note_list = Note.objects.all()
    response = {'note_list': note_list,
            }
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)

    if request.method == "POST" and form.is_valid():
        form.save()
        return redirect('/lab-4/')

    response = {'form': form}
    
    return render(request, 'lab4_form.html', response)

def note_list(request):
    note_list = Note.objects.all()
    response = {'note_list': note_list,
            }
    return render(request, 'lab4_note_list.html', response)