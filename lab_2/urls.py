from django.urls import path, include
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'),
    path('json', json, name='json'),
    path('xml', xml, name='xml'),
]
