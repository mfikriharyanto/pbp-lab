class Kegiatan {
  final String id;
  final String nama;
  final String deskripsi;

  const Kegiatan({
    required this.id,
    required this.nama,
    required this.deskripsi,
  });
}
